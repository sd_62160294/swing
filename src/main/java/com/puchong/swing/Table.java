/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puchong.swing;

import java.io.Serializable;

/**
 *
 * @author User
 */
public class Table implements Serializable{


    private char[][] table = {{'-', '-', '-'},{'-', '-', '-'},{'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player win;
    private boolean finish = false;
    private int lastCol;
    private int lastRow;
    private int round = 0;
    
    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }
    public void showTable() {
        System.out.println("  1 2 3");
        for(int i=0; i<table.length; i++) {
            System.out.print(i + " ");
            for(int j=0; j<table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }
    public char getRowCol(int row,int col){
        return table[row][col];
    }
    public boolean setRowCol(int row, int col) {
        if(isFinish()) return false;
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastRow = row;
            this.lastCol = col;
            checkWin();
            return true;
        }
        return false;
    }
    public Player getCurrentPlayer() {
        return currentPlayer;
    }
    public void switchPlayer() {
        if(currentPlayer==playerX) {
            currentPlayer = playerO;
            round++;
        } else{
            currentPlayer = playerX;
            round++;
        }
    }
    private void checkWinLoseDraw() {
        if(currentPlayer==playerX) {
            playerX.win();
            playerO.lose();
        } else {
            playerX.lose();
            playerO.win(); 
        }
    }
    void checkCol() {
        for(int row=0; row<3; row++) {
            if(table[row][lastCol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        win = currentPlayer;
        checkWinLoseDraw();
    }
    void checkRow() {
        for(int col=0; col<3; col++) {
            if(table[lastRow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        win = currentPlayer;
        checkWinLoseDraw();
    }
    void checkX() {
        for (int col = 0; col < 3; col++) {
            if (table[col][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        win = currentPlayer;
        checkWinLoseDraw();   
    }
    void checkY() {
        for (int row = 0, count = 2; row < 3; row++, count--) {
            if (table[row][count] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        win = currentPlayer;
        checkWinLoseDraw();
    }
    public void countRound(){
        round++;
    }

    void checkDraw() {
        if (round == 8 && isFinish() == false) {
            playerX.draw();
            playerO.draw();
            finish = true;
            win = null;
        }
    }
    public void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkY();
        checkDraw();
    }
    
    public boolean isFinish() {
        return finish;
    }
    public Player getWin() {
        return win;
    }

}
